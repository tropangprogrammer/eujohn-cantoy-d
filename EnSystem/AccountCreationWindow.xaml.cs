﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.OleDb;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for AccountCreationWindow.xaml
    /// </summary>
    public partial class AccountCreationWindow : Window
    {
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= .\database\EnrollSchoolDb.accdb;");
        OleDbCommand cmd = new OleDbCommand();
        string accountDefault = "No";

        public AccountCreationWindow()
        {
            InitializeComponent();
        }

        private void ACbar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            loginWindow lw = new loginWindow();
            lw.Show();
            this.Close();
        }

        private void Create_Account_Click(object sender, RoutedEventArgs e)
        {
            if (tb_Contact.Text != "" && tb_Fname.Text != "" && tb_IDnumber.Text != ""
                && tb_Lname.Text != "" && tb_UserName.Text != "" && pass1.Password != "" && pass2.Password != "")
            {
                if (pass1.Password == pass2.Password)
                {
                    try
                    {
                        conn.Open();
                        cmd.CommandText = "Insert into tbl_Info(i_fname,i_Lname,i_contactNum,i_IdNumber)Values('"+tb_Fname.Text+ "','" + tb_Lname.Text + "','" + tb_Contact.Text + "','" + tb_IDnumber.Text + "')";
                        cmd.Connection = conn;
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        conn.Open();
                        cmd.CommandText = "Insert into tbl_Login(e_usern,e_passw,i_IdNumber,e_is_admin)Values('" + tb_UserName.Text + "','" + pass2.Password + "','" + tb_IDnumber + "','"+accountDefault+"')";
                        cmd.ExecuteNonQuery();
                        conn.Close();
                        MessageBox.Show("Account Created by " + tb_Fname.Text + " " + tb_Lname.Text);
                    }
                    catch(Exception except)
                    {
                        MessageBox.Show(except.Message, "Error");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Password Mismatch");
                }

            }
            else
            {
                MessageBox.Show("Answer the required field");
            }
        }
    }  
}
