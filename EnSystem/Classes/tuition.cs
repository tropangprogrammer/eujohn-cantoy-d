﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace EnSystem
{
    public class tuition:MainWindow
    {
        public static int c_miscfee = 1500, c_labFee = 1000, c_tuition;
        public static double c_increase,c_total;
        public static string c_newresult, c_firstname, c_lastname, c_yearlevel, c_status, c_course, c_contact, c_idnum;
        public static void tuitionfee()
        {
                if (c_yearlevel == "1st Year")
                {
                    if (c_status == "New Student" + "Old Student")
                    {
                        c_tuition = 10000;
                        compute();
                    }
                    else
                    {
                        MessageBox.Show("Invalid Status");
                    }
                }
                else if (c_yearlevel == "2nd Year")
                {
                    c_tuition = 9000;
                    if (c_status == "New Student" + "Transfer Student")
                    {
                        c_increase = c_tuition * .12;
                        compute();
                    }
                    else if (c_status == "Old Student")
                    {
                        c_increase = c_tuition * .10;
                        compute();
                    }
                }
                else if (c_yearlevel == "3rd Year")
                {
                    c_tuition = 8000;
                    if (c_status == "New Student" + "Transfer Student")
                    {
                        c_increase = c_tuition * .12;
                        compute();
                    }
                    else if (c_status == "Old Student")
                    {
                        c_increase = c_tuition * .10;
                        compute();
                    }
                }
                else if (c_yearlevel == "4th Year")
                {
                    c_tuition = 7000;
                    if (c_status == "New Student" + "Transfer Student")
                    {
                        c_increase = c_tuition * .12;
                        compute();
                    }
                    else if (c_status == "Old Student")
                    {
                        c_increase = c_tuition * .10;
                        compute();
                    }
                }
                else if (c_yearlevel == "5th Year")
                {

                    c_tuition = 6000;
                    if (c_status == "New Student" + "Transfer Student")
                    {
                        c_increase = c_tuition * .12;
                        compute();
                    }
                    else if (c_status == "Old Student")
                    {
                        c_increase = c_tuition * .10;
                        compute();
                    }
                }
            }

        private static void compute()
        {
            c_total = (c_miscfee+c_labFee+c_tuition)+c_increase; 
        }
            
    }
        
}
