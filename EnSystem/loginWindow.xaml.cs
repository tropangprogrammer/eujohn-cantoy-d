﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.OleDb;
using System.Data;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for loginWindow.xaml
    /// </summary>
    public partial class loginWindow : Window
    {
        //string connString = @"Provider=Microsoft.Ace.OLEDB.12.0;Data Source = C:\EXERCISE\EnrollSchoolDb.mdb";
        public OleDbConnection conn = new OleDbConnection();
        public int count;
        MessageBoxResult LoginResult = new MessageBoxResult();
        public static string currentUser,pass;
        string IsAdmin = "Yes";
        
        public loginWindow()
        {
            
            InitializeComponent();
            conn.ConnectionString = @"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= .\database\EnrollSchoolDb.accdb;";

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //_typedInto = !String.IsNullOrEmpty(Txtbox_username.Text);
        }

        private void Tab_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            if (Txtbox_username.Text != "" && password.Password != "")
            {
             
                try
                {
                    conn.Open();
                    string selectUser = @"SELECT * FROM tbl_Login WHERE e_usern = '" + Txtbox_username.Text + "' AND e_passw = '" + password.Password + "' AND e_is_admin = '"+IsAdmin+"';";
                    OleDbCommand cmd = new OleDbCommand (selectUser);
                    cmd.Connection = conn;
                    //string selectAdmin = @"SELECT * FROM tbl_Login WHERE e_usern = '" + Txtbox_username.Text + "' AND e_passw = '" + password.Password + "';"
                    OleDbDataReader read = cmd.ExecuteReader();

                    if (read.HasRows)
                    {
                        LoginResult = MessageBox.Show("Password and Username correct", "access granted", MessageBoxButton.OK);
                        if (LoginResult == MessageBoxResult.OK)
                        {
                            currentUser = Txtbox_username.Text;
                            pass = password.Password;
                            conn.Close();
                            this.Hide();
                            MainWindow mn = new MainWindow();
                            mn.Show();
                            this.Close();
                        }

                    }
                    else
                    {
                        count++;
                        
                        if (count >= 1 && count != 3)
                        {
                            LoginResult = MessageBox.Show("Wrong Username or Password");

                        }
                        else 
                        {
                            LoginResult = MessageBox.Show("you attempted " + count + " program will now exit");
                            Environment.Exit(0);

                        }

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Please Contact administrator : Error Detail : " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Username and Password required");
            }


        }

        private void l_window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           e.Cancel = true;
        }

        private void close_login_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Txtbox_password_TextChanged(object sender, TextChangedEventArgs e)
        {
            //_typedInto = !string.IsNullOrEmpty(Txtbox_password.Text);
        }

        private void l_window_Loaded(object sender, RoutedEventArgs e)
        {
         
        }

        private void create_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            AccountCreationWindow Acw = new AccountCreationWindow();
            Acw.Show();
            this.Close();
        }
    }
}
