﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Data.OleDb;
using System.Windows.Threading;
using System.Data;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        //public string Path = Application.StartupPath + "\\db_Enrollment.accdb";

        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= .\database\EnrollSchoolDb.accdb;");
        OleDbCommand cmd = new OleDbCommand();
        

        public MainWindow()
        {
            InitializeComponent();
            name_Label.Content = "Hello "+ loginWindow.currentUser;
            conn.Open();
            string select = "SELECT tbl_Info.i_fname +' '+ tbl_Info.i_Lname as Name, tbl_Login.i_IdNumber as Id_Number, tbl_Login.e_usern as Username FROM tbl_Info INNER JOIN tbl_Login ON tbl_Info.i_IdNumber = tbl_Login.i_IdNumber Where tbl_Login.e_usern = '" + loginWindow.currentUser + "'";
            var dtEnrolled = new DataTable();
            var adapter = new OleDbDataAdapter(select,conn);
            adapter.Fill(dtEnrolled);
            datagrid_db.ItemsSource = dtEnrolled.AsDataView();
            datagrid_db.Items.Refresh();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            


       }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void txtbox_fname_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_lname_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_idnum_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_course_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_contact_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void combo_yearlevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (combo_yearlevel.Text != "")
            {
                combo_status.IsEnabled = true;
            }
            else
            {
                combo_status.Text = "";

                combo_status.IsEnabled = false;
            }
        }

        private void combo_status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult dialog = new MessageBoxResult();

            dialog = MessageBox.Show("Are you sure you want to quit?", "Closing Application", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (dialog == MessageBoxResult.OK)
            {
                Application.Current.Shutdown();
            }
        }

        private void datagrid_db_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void b_logout_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult logout = new MessageBoxResult();
            logout = MessageBox.Show("Logout?","Logging Out",MessageBoxButton.OKCancel,MessageBoxImage.Question);
            if(logout == MessageBoxResult.OK)
            {
                loginWindow gotologin = new loginWindow();
                this.Hide();
                gotologin.Show();
                this.Close();

            }
          
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        void timer_Tick(object sender, EventArgs e)
        {
            test.Content = DateTime.Now;
        }
    }
}
