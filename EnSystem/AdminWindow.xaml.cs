﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EnSystem.AdminWindowViews.Views;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Admin_Bar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_User_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AdminManageUsers_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminManageUsers();
        }

        private void AdminManageEnroll_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminManageEnroll();
        }

        private void AdminModifyPayment_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminModifyPayment();
        }
    }
}
